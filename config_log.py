import sys
from os import environ

CONFIG_LOGGING = {
    'version': 1,
    'formatters': { 
        'standard': {
            'format': f'%(asctime)s service:sistema_libreria containerid:{environ.get("HOSTNAME")} level:%(levelname)s: %(message)s',
            'datefmt': '%Y-%m-%d - %H:%M:%S' },
    },
    'handlers': {
        'console':  {'class': 'logging.StreamHandler', 
                    'formatter': "standard", 
                    'level': 'DEBUG', 
                    'stream': sys.stdout},
        'file_messagequeuer':     {'class': 'logging.FileHandler', 
                    'formatter': "standard", 
                    'level': 'DEBUG', 
                    'filename': 'log/messagequeuer.log','mode': 'a'} ,
        'file_senderrequest':     {'class': 'logging.FileHandler', 
                    'formatter': "standard", 
                    'level': 'DEBUG', 
                    'filename': 'log/sendrequest.log','mode': 'a'} 
    },
    'loggers': { 
        'MESSAGEQUEUER_LOGGING':   {'level': 'INFO', 
                    'handlers': ['console', 'file_messagequeuer'], 
                    'propagate': False },
        'SENDREQUEST_LOGGING':   {'level': 'INFO', 
                    'handlers': ['console', 'file_senderrequest'], 
                    'propagate': False }
    }
}