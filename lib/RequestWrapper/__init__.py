import requests
import logging

class RequestWrapper():
    log = logging.getLogger('SENDREQUEST_LOGGING')

    @classmethod
    def get(cls, url, params=None, **kwargs):
        cls.log.info(f"[Saliente] Request. request.method=GET request.url={url}")
        r = requests.get(url, params, **kwargs)
        data = r.text.replace('\n', '')
        cls.log.info(f"[Saliente] Response. response.method=GET response.url={url} response.data='{data}' response.status_code={r.status_code}")
        return r

    @classmethod
    def post(cls, url, params=None, **kwargs):
        body = str(kwargs.get('json', '')).replace('\n', '')
        cls.log.info(f"[Saliente] Request. request.method=POST request.url={url} request.data={body}")
        r = requests.post(url, params, **kwargs)
        data = r.text.replace('\n', '')
        cls.log.info(f"[Saliente] Response. response.method=POST response.url={url} response.data='{data}' response.status_code={r.status_code}")
        return r

    @classmethod
    def put(cls, url, params=None, **kwargs):
        body = str(kwargs.get('json', '')).replace('\n', '')
        cls.log.info(f"[Saliente] Request. request.method=PUT request.url={url} request.data={body}")
        r = requests.put(url, params, **kwargs)
        data = r.text.replace('\n', '')
        cls.log.info(f"[Saliente] Response. response.method=PUT response.url={url} response.data='{data}' response.status_code={r.status_code}")
        return r