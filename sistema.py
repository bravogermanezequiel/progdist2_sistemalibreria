import pika
import json
import itertools
from datetime import datetime
from config_uri import URL_ENVIOS, URL_ORDENES, HOST_AMQP
from config_log import CONFIG_LOGGING
import logging
import logging.config
from lib.RequestWrapper import RequestWrapper

logging.config.dictConfig(CONFIG_LOGGING)
log = logging.getLogger('MESSAGEQUEUER_LOGGING')

MAX_WEEKS = 5

def getTransportesPerWeek(week):
    URL = f"{URL_ENVIOS}/transportes/dates/{week}"
    r = RequestWrapper.get(URL)

    return json.loads(r.text)

def getTransportes():
    transportes = []
    for week in range(1,MAX_WEEKS+1):
        transportes += getTransportesPerWeek(week)

    return transportes

def processFechasTransportes(transportes):
    allDatesStr = [ 
        list(map(
            lambda t: t[3], 
            transporte["disponibilidadesHorarias"])
        ) for transporte in transportes
    ]
    datesStr = list(set(itertools.chain(*allDatesStr)))
    dates = list(map(lambda s: datetime.strptime(s, "%d-%m-%Y"), datesStr))
    sortDates = sorted(dates)
    dates = list(map(lambda d: d.strftime("%d-%m-%Y"), sortDates))
    return dates

def processTransportesPorFechas(fechas, transportes):
    transportesByDate = { fecha: [] for fecha in fechas }
    for fecha in fechas:
        for transporte in transportes:
            if any( [fecha in list(map(lambda dh: dh[3], transporte["disponibilidadesHorarias"]))]):
                transportesByDate[fecha] += [transporte["patente"]]
    return transportesByDate

def updateOrdenProcesada(ordenId, envioId):
    r = RequestWrapper.get(f"{URL_ORDENES}/orden/{ordenId}")
    if r.status_code == 200:
        orden = json.loads(r.text)
        orden["procesado"] = True
        orden["envioId"] = envioId

        r = RequestWrapper.put(f"{URL_ORDENES}/orden/{ordenId}", json=orden)

def crearEnvio(envio):
    r = RequestWrapper.post(f"{URL_ENVIOS}/envio", json=envio)
    sent = (r.status_code == 201)
    envioId = json.loads(r.text).get("id")

    return sent, envioId

def updateEnvio(envio):
    r = RequestWrapper.put(f"{URL_ENVIOS}/envio/{envio['id']}", json=envio)
    sent = (r.status_code == 200)
    envioId = envio.get("id")

    return sent, envioId

def filtrarEnvio(patente, fecha):
    r = RequestWrapper.get(f"{URL_ENVIOS}/envio/{patente}/{fecha}")

    return r.status_code, r.text

def processMessage(message, fechas, transportes, transportesByDate):
    sent = False
    envioId = None
    for fecha in fechas:
        if sent:
            break
        for patente in transportesByDate[fecha]:
            if sent:
                break
            statusCode, text = filtrarEnvio(patente, fecha)
            if statusCode == 404:
                envio = {
                    "patente": patente,
                    "fecha": fecha,
                    "ordenes": [ message.get("id") ]
                }

                sent, envioId = crearEnvio(envio)
            elif statusCode == 200:
                envio = json.loads(text)
                envio["ordenes"] += [ message.get("id") ]

                sent, envioId = updateEnvio(envio)
    
    return sent, envioId

def onMessage(ch, method, pr, body):
    transportes = getTransportes()
    fechas = processFechasTransportes(transportes)
    transportesByDate = processTransportesPorFechas(fechas, transportes)
    message = json.loads(body)

    sent, envioId = processMessage(message, fechas, transportes, transportesByDate)

    if not sent:
        ch.basic_nack(method.delivery_tag)
        log.info(f"Consumo mensaje fallido, encolando nuevamente method.delivery_tag={method.delivery_tag} body='{body}'")
    else:
        ch.basic_ack(method.delivery_tag, False)
        log.info(f"Consumo mensaje exitoso method.delivery_tag={method.delivery_tag} body='{body}'")
        updateOrdenProcesada(message.get("id"), envioId)
    
def consume(connection):
    channel = connection.channel()
    result = channel.queue_declare(queue='ordenes', auto_delete=False)
    queue_name = result.method.queue
    log.info(f"Cola declarado queue.name={queue_name}")
    channel.queue_bind(exchange='ordenes', queue=queue_name)
    log.info(f"Cola vinculada queue.name={queue_name} exchange.name=ordenes")
    channel.basic_consume(queue=queue_name, on_message_callback=onMessage, auto_ack=False)
    try:
        channel.start_consuming()
        log.info(f"Consumo de cola inicializado queue.name={queue_name}")
    except pika.exceptions.ChannelClosed:
        channel.stop_consuming()
        log.info(f"Consumo de cola finalizado queue.name={queue_name}")
        consume(connection)

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(HOST_AMQP))
    consume(connection)
    log.info(f"Conexion iniciada. host.name={HOST_AMQP}")
    connection.close()

if __name__ == "__main__":
    main()